#pragma once
#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "string.h"

int IsInt(const char *s);

typedef struct SEqInt
{
	char *value;
	int OpNo;
	SEqInt *sub1;
	SEqInt *sub2;
	
}SEqInt;

typedef struct SEqDouble
{
	char *value;
	int OpNo;
	SEqDouble *sub1;
	SEqDouble *sub2;
}SEqDouble;

int IntEval(SEqInt *x);
double DoubleEval(SEqDouble *x);
int CreateSEqInt(SEqInt *x, const char *s);
int CreateSEqDouble(SEqDouble *x, const char *s);
void DeleteSEqDouble(SEqDouble *x);
void DeleteSEqInt(SEqInt *x);
int IsInt(const char *s);
int IsDouble(const char *s);
int insert(const char *x, int n1, int n2, char **dest);
int Trim(char **s);
