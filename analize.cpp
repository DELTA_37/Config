#include "analize.h"

int IsInt(const char *s)
{
	int n = strlen(s);
	int i;
	for (i = 0; i <= n - 1; i++)
	{
		if (i == 0)
		{
			if ((s[i] != '-')&&((s[i] < '0')||(s[i] > '9')))
			{
				return 0;
			}
		}
		else if ((s[i] < '0')||(s[i] > '9'))
		{
			return 0;
		}
	}
	return 1;
}

int Trim(char **s)
{
	char *res;
	res = *s;
	int i;
	int j = 0;
	char *dop = new char[strlen(res) + 1];
	for (i = 0; i <= strlen(res); i++)
	{
		if (res[i] != ' ')
		{
			dop[j] = res[i];
			j++;
		}
	}
	dop[j] = '\0';
	delete[] *s;
	*s = dop;
}

int IsDouble(const char *s)
{
	if (s[0] == '-')
	{
		return IsDouble(s + 1);
	}
	else
	{
		int b1 = 0;
		int b2 = 0;
		int i;
		for (i = 0; i <= strlen(s) - 1; i++)
		{
			if (s[i] == '.')
			{
				int j;
				char *dop;
				dop = new char[i + 1];
				for (j = 0; j <= i - 1; j++)
				{
					dop[j] = s[j];
				}
				dop[i] = '\0';
				b1 = IsInt(dop);
				b2 = IsInt(s + i);
				delete[] dop;
				return b1 & b2;
			}
		}
		return IsInt(s);
	}
}

int IntEval(SEqInt *x)
{
	if ((x->sub1 == NULL)||(x->sub2 == NULL))
	{
		int a;
		sscanf(x->value, "%d", &a);
		return a;
	}
	else
	{
		int x1;
		int x2;
		x1 = IntEval(x->sub1);
		x2 = IntEval(x->sub2);
		switch (x->OpNo)
		{
			case 1 : 
			{
				return x1 + x2;
				break;
			}
			case 2 :
			{
				return x1 - x2;
				break;
			}
			case 3 :
			{
				return x1 * x2;
				break;
			}
			case 4 :
			{
				return x1/x2;
				break;
			}
			default :
			{
				throw -1;
			}
		}

	}
}

double DoubleEval(SEqDouble *x)
{
	if ((x->sub1 == NULL)||(x->sub2 == NULL))
	{
		double a;
		sscanf(x->value, "%lf", &a);
		return a;
	}
	else
	{
		double x1;
		double x2;
		x1 = DoubleEval(x->sub1);
		x2 = DoubleEval(x->sub2);
		switch (x->OpNo)
		{
			case 1 : 
			{
				return x1 + x2;
				break;
			}
			case 2 :
			{
				return x1 - x2;
				break;
			}
			case 3 :
			{
				return x1 * x2;
				break;
			}
			case 4 :
			{
				return x1/x2;
				break;
			}
			default :
			{
				throw -1;
			}
		}

	}
}

int CreateSEqInt(SEqInt *x, const char *s)
{
	if (IsInt(s) == 1)
	{
		int n = strlen(s);
		int i;
		x->sub1 = NULL;
		x->sub2 = NULL;
		x->value = new char[n + 1];
		for (i = 0; i <= n - 1; i++)
		{
			x->value[i] = s[i];
		}
		x->value[n] = '\0';
		return 0;
	}
	else
	{
		int i;
		int n = strlen(s);
		int k = 0;
		x->value = new char[n + 1];
		for (i = 0; i <= n - 1; i++)
		{
			x->value[i] = s[i];
		}
		x->value[n] = '\0';
		for (i = 0; i <= n - 1; i++)
		{
			if (s[i] == '(')
			{
				k++;
			}
			else if (s[i] == ')')
			{
				k--;
			}
			if (k == 0)
			{
				if (i == 0)
				{
					return -1;
				}
				else
				{
					switch (s[i + 1])
					{
						case '+' :
						{
							int j;
							int c1, c2;
							x->OpNo = 1;
							x->sub1 = (SEqInt *)malloc(sizeof(SEqInt));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqInt(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqInt *)malloc(sizeof(SEqInt));
							dop = new char[n - i - 1];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqInt(x->sub2, dop);
							delete[] dop;
							return c1*c2;
						}
						case '-' :
						{
							int j;
							int c1, c2;
							x->OpNo = 2;
							x->sub1 = (SEqInt *)malloc(sizeof(SEqInt));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqInt(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqInt *)malloc(sizeof(SEqInt));
							dop = new char[n - i];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqInt(x->sub2, dop);
							delete[] dop;
							return c1*c2;
						}
						case '*' :
						{
							int j;
							int c1, c2;
							x->OpNo = 3;
							x->sub1 = (SEqInt *)malloc(sizeof(SEqInt));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqInt(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqInt *)malloc(sizeof(SEqInt));
							dop = new char[n - i];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqInt(x->sub2, dop);
							delete[] dop;
							return c1 * c2;
						}
						case '/' :
						{
							int j;
							int c1, c2;
							x->OpNo = 4;
							x->sub1 = (SEqInt *)malloc(sizeof(SEqInt));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqInt(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqInt *)malloc(sizeof(SEqInt));
							dop = new char[n - i];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqInt(x->sub2, dop);
							delete[] dop;
							return c1*c2;
						}
					}
				}
			}
		}
		return -1;
	}
}

int CreateSEqDouble(SEqDouble *x, const char *s)
{
	if (IsDouble(s) == 1)
	{
		int n = strlen(s);
		int i;
		x->sub1 = NULL;
		x->sub2 = NULL;
		x->value = new char[n + 1];
		for (i = 0; i <= n - 1; i++)
		{
			x->value[i] = s[i];
		}
		x->value[n] = '\0';
		return 0;
	}
	else
	{
		int i;
		int n = strlen(s);
		int k = 0;
		x->value = new char[n + 1];
		for (i = 0; i <= n - 1; i++)
		{
			x->value[i] = s[i];
		}
		x->value[n] = '\0';
		for (i = 0; i <= n - 1; i++)
		{
			if (s[i] == '(')
			{
				k++;
			}
			else if (s[i] == ')')
			{
				k--;
			}
			if (k == 0)
			{
				if (i == 0)
				{
					return -1;
				}
				else
				{
					switch (s[i + 1])
					{
						case '+' :
						{
							int j;
							int c1, c2;
							x->OpNo = 1;
							x->sub1 = (SEqDouble *)malloc(sizeof(SEqDouble));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqDouble(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqDouble *)malloc(sizeof(SEqDouble));
							dop = new char[n - i - 1];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqDouble(x->sub2, dop);
							delete[] dop;
							return c1*c2;
						}
						case '-' :
						{
							int j;
							int c1, c2;
							x->OpNo = 2;
							x->sub1 = (SEqDouble *)malloc(sizeof(SEqDouble));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqDouble(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqDouble *)malloc(sizeof(SEqDouble));
							dop = new char[n - i];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqDouble(x->sub2, dop);
							delete[] dop;
							return c1*c2;
						}
						case '*' :
						{
							int j;
							int c1, c2;
							x->OpNo = 3;
							x->sub1 = (SEqDouble *)malloc(sizeof(SEqDouble));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqDouble(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqDouble *)malloc(sizeof(SEqDouble));
							dop = new char[n - i];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqDouble(x->sub2, dop);
							delete[] dop;
							return 0;
						}
						case '/' :
						{
							int j;
							int c1, c2;
							x->OpNo = 4;
							x->sub1 = (SEqDouble *)malloc(sizeof(SEqDouble));
							char *dop;
							dop = new char[i + 2];
							for (j = 0; j <= i - 2; j++)
							{
								dop[j] = s[j + 1];
							}
							dop[i] = '\0';
							c1 = CreateSEqDouble(x->sub1, dop);
							delete[] dop;

							x->sub2 = (SEqDouble *)malloc(sizeof(SEqDouble));
							dop = new char[n - i];
							for (j = 0; j <= n - i - 5; j++)
							{
								dop[j] = s[i + j + 3];
							}
							dop[n - i - 2] = '\0';
							c2 = CreateSEqDouble(x->sub2, dop);
							delete[] dop;
							return 0;
						}
					}
				}
			}
		}
		return -1;
	}
}

void DeleteSEqInt(SEqInt *x)
{
	if (x != NULL)
	{
		if (x->sub1 != NULL)
		{
			DeleteSEqInt(x->sub1);
			delete x->sub1;
		}
		if (x->sub2 != NULL)
		{
			DeleteSEqInt(x->sub2);
			delete x->sub2;
		}
		if (x->value != NULL)
		{
			delete[] x->value;
		}
	}
}

void DeleteSEqDouble(SEqDouble *x)
{
	if (x != NULL)
	{
		if (x->sub1 != NULL)
		{
			DeleteSEqDouble(x->sub1);
			delete x->sub1;
		}
		if (x->sub2 != NULL)
		{
			DeleteSEqDouble(x->sub2);
			delete x->sub2;
		}
		if (x->value != NULL)
		{
			delete[] x->value;
		}
	}
}
int insert(const char *x, int n1, int n2, char **dest)
{
	char *s = (*dest);
	int i;
	volatile int LenS = strlen(s);
	volatile int LenX = strlen(x);
	int n = n1 - n2 + LenX + LenS - 2;
	char *dop = new char[n + 2];
	// 0 ... n1 - 1 - str   n1 ... n2                 - del    n2 + 1         ... length(s) - 1                       - str
	// 0 ... n1 - 1 - str   n1 ... n1 + length(x) - 1 - ins    n1 + length(x) ... n1 + length(x) + length(s) - n2 - 2 - str
	for (i = 0; i <= n1 - 1; i++)
	{
		dop[i] = s[i];
	}
	for (i = 0; i <= LenX - 1; i++)
	{
		dop[n1 + i] = x[i];
	}
	for (i = 0; i <= LenS - n2 - 2; i++)
	{
		dop[n1 + LenX + i] = s[n2 + 1 + i];
	}
	dop[n + 1] = '\0';
	delete[] (*dest);
	(*dest) = new char[n + 2];
	for (i = 0; i <= n + 1; i++)
	{
		(*dest)[i] = dop[i];
	}
	delete[] dop;
	return 0;
}



