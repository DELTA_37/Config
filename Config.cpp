#include "Config.h"
#include "analize.h"

int CConfig::AddInt(const char *s, int x)
{
	if (this->IntK == IntN)
	{
		printf("AddIntMax");
		return -1;
	}
	else
	{
		this->IntName[IntK] = new char[strlen(s) + 1];
		memcpy(this->IntName[IntK], s, strlen(s) + 1);
		this->Ints[IntK] = x;
		this->IntK++;
		return 0;
	}
}

int CConfig::AddDouble(const char *s, double x)
{
	if (DoubleK == DoubleN)
	{
		printf("AddDoubleMax");
		return -1;
	}
	else
	{
		this->DoubleName[DoubleK] = new char[strlen(s) + 1];
		memcpy(this->DoubleName[DoubleK], s, strlen(s) + 1);
		this->Doubles[DoubleK] = x;
		DoubleK++;
		return 0;
	}
}

int CConfig::AddString(const char *s, const char *x)
{
	if (StringK == StringN)
	{
		printf("AddStringMax");
	}
	else
	{
		this->StringName[StringK] = new char[strlen(s) + 1];
		memcpy(this->StringName[StringK], s, strlen(s) + 1);
		this->Strings[StringK] = new char[strlen(x) + 1];
		memcpy(this->StringName[StringK], x, strlen(x) + 1);
		StringK++;
		return 0;
	}
}

int CConfig::SetInt(const char *s, int x)
{
	int i;
	for (i = 0; i <= IntK - 1; i++)
	{
		if (strlen(s) == strlen(IntName[i]))
		{
			if (strncmp(s, IntName[i], strlen(s)) == 0)
			{
				Ints[i] = x;
			}
		}
	}
	return 0;
}

int CConfig::SetDouble(const char *s, double x)
{
	int i;
	for (i = 0; i <= DoubleK - 1; i++)
	{
		if (strlen(s) == strlen(DoubleName[i]))
		{
			if (strncmp(s, DoubleName[i], strlen(s)) == 0)
			{
				Doubles[i] = x;
			}
		}
	}
	return 0;
}

int CConfig::SetString(const char *s, const char *x)
{
	int i;
	for (i = 0; i <= StringK - 1; i++)
	{
		if (strlen(s) == strlen(StringName[i]))
		{
			if (strncmp(s, StringName[i], strlen(s)) == 0)
			{
				int j;
				delete[] Strings[i];
				Strings[i] = new char[strlen(x) + 1];
				for (j = 0; j <= strlen(x) - 1; j++)
				{
					Strings[i][j] = x[j];
				}
				Strings[strlen(x)] = '\0';
			}
		}
	}
	return 0;
}

int CConfig::GetInt(const char *s, int *x)
{
	int i;
	for (i = 0; i <= IntK - 1; i++)
	{
		if (strlen(s) == strlen(IntName[i]))
		{
			if (strncmp(s, IntName[i], strlen(s)) == 0)
			{
				(*x) = Ints[i];
			}
		}
	}
	return 0;
}

int CConfig::GetDouble(const char *s, double *x)
{
	int i;
	for (i = 0; i <= DoubleK - 1; i++)
	{
		if (strlen(s) == strlen(DoubleName[i]))
		{
			if (strncmp(s, DoubleName[i], strlen(s)) == 0)
			{
				(*x) = Doubles[i];
			}
		}
	}
	return 0;
}

int CConfig::GetString(const char *s, char **x)
{
	int i;
	for (i = 0; i <= StringK - 1; i++)
	{
		if (strlen(s) == strlen(StringName[i]))
		{
			if (strncmp(s, StringName[i], strlen(s)) == 0)
			{
				int j;
				(*x) = new char[strlen(Strings[i]) + 1];
				for (j = 0; j <= strlen(Strings[i]) - 1; j++)
				{
					(*x)[j] = Strings[i][j];
				}
				(*x)[strlen(Strings[i])] = '\0';
			}
		}
	}
	return 0;
}

int CConfig::WhatType(const char *s)
{
	int i;
	for (i = 0; i <= IntK - 1; i++)
	{
		if (strlen(s) == strlen(IntName[i]))
		{
			if (strncmp(s, IntName[i], strlen(s)) == 0)
			{
				return 1;
			}
		}
	}
	for (i = 0; i <= DoubleK - 1; i++)
	{
		if (strlen(s) == strlen(DoubleName[i]))
		{
			if (strncmp(s, DoubleName[i], strlen(s)) == 0)
			{
				return 2;
			}
		}
	}
	for (i = 0; i <= StringK - 1; i++)
	{
		if (strlen(s) == strlen(StringName[i]))
		{
			if (strncmp(s, StringName[i], strlen(s)) == 0)
			{
				return 3;
			}
		}
	}
	return 0;
}

CConfig CConfig::operator+(const CConfig &Q)
{
	int i, j;
	CConfig R(this->IntN + Q.IntN, this->DoubleN + Q.DoubleN, this->StringN + Q.StringN);
	for (i = 0; i <= Q.IntK - 1; i++)
	{
		R.IntName[i] = new char[strlen(Q.IntName[i]) + 1];
		strcpy(R.IntName[i], Q.IntName[i]);
		R.IntName[strlen(Q.IntName[i])] = '\0';
		R.Ints[i] = Q.Ints[i];
		R.IntK++;
	}
	for (i = 0; i <= this->IntK - 1; i++)
	{
		int bl = 1;
		for (j = 0; j <= R.IntK - 1; j++)
		{
			if (strlen(this->IntName[i]) == strlen(R.IntName[j]))
			{
				if (strcmp(R.IntName[j], this->IntName[i]) == 0)
				{
					bl = 0;
					break;
				}
			}
		}
		if (bl == 1)
		{
			R.IntName[R.IntK] = new char[strlen(this->IntName[i]) + 1];
			strcpy(R.IntName[R.IntK], this->IntName[i]);
			R.IntName[R.IntK][strlen(this->IntName[i]) + 1] = '\0';
			R.Ints[R.IntK] = this->Ints[i];
			R.IntK++;
		}
	}
	for (i = 0; i <= Q.DoubleK - 1; i++)
	{
		R.DoubleName[i] = new char[strlen(Q.DoubleName[i]) + 1];
		strcpy(R.DoubleName[i], Q.DoubleName[i]);
		R.DoubleName[strlen(Q.DoubleName[i])] = '\0';
		R.Doubles[i] = Q.Doubles[i];
		R.DoubleK++;
	}
	for (i = 0; i <= this->DoubleK - 1; i++)
	{
		int bl = 1;
		for (j = 0; j <= R.DoubleK - 1; j++)
		{
			if (strlen(this->DoubleName[i]) == strlen(R.DoubleName[j]))
			{
				if (strcmp(R.DoubleName[j], this->DoubleName[i]) == 0)
				{
					bl = 0;
					break;
				}
			}
		}
		if (bl == 1)
		{
			R.DoubleName[R.DoubleK] = new char[strlen(this->DoubleName[i]) + 1];
			strcpy(R.DoubleName[R.DoubleK], this->DoubleName[i]);
			R.DoubleName[R.DoubleK][strlen(this->DoubleName[i]) + 1] = '\0';
			R.Doubles[R.DoubleK] = this->Doubles[i];
			R.DoubleK++;
		}
	}
	for (i = 0; i <= Q.StringK - 1; i++)
	{
		R.StringName[i] = new char[strlen(Q.StringName[i]) + 1];
		strcpy(R.StringName[i], Q.StringName[i]);
		R.StringName[strlen(Q.StringName[i])] = '\0';
		R.Strings[i] = new char[strlen(Q.Strings[i]) + 1];
		strcpy(R.Strings[i], Q.Strings[i]);
		R.Strings[i][strlen(Q.Strings[i])] = '\0';
		R.StringK++;
	}
	for (i = 0; i <= this->StringK - 1; i++)
	{
		int bl = 1;
		for (j = 0; j <= R.StringK - 1; j++)
		{
			if (strlen(this->StringName[i]) == strlen(R.StringName[j]))
			{
				if (strcmp(R.StringName[j], this->StringName[i]) == 0)
				{
					bl = 0;
					break;
				}
			}
		}
		if (bl == 1)
		{
			R.StringName[R.StringK] = new char[strlen(this->StringName[i]) + 1];
			strcpy(R.StringName[R.StringK], this->StringName[i]);
			R.StringName[R.StringK][strlen(this->StringName[i]) + 1] = '\0';
			R.Strings[R.StringK] = new char[strlen(this->Strings[i]) + 1];
			strcpy(R.Strings[R.StringK], this->Strings[i]);
			R.Strings[R.StringK][strlen(this->Strings[i])] = '\0';
			R.StringK++;
		}
	}
	return R;
}

CConfig::CConfig(const CConfig &Q)
{
	int i;
	this->IntK = Q.IntK;
	this->DoubleK = Q.DoubleK;
	this->StringK = Q.StringK;
	this->IntN = Q.IntN;
	this->DoubleN = Q.DoubleN;
	this->StringN = Q.StringN;
	this->StringName = new char*[Q.StringN];
	this->IntName = new char*[Q.IntN];
	this->DoubleName = new char*[Q.DoubleN];
	this->Ints = new int[Q.IntN];
	this->Doubles = new double[Q.DoubleN];
	this->Strings = new char*[Q.StringN];
	for (i = 0; i <= Q.StringK - 1; i++)
	{
		int j;
		this->Strings[i] = new char[strlen(Q.Strings[i]) + 1];
		for (j = 0; j <= strlen(Q.Strings[i]); j++)
		{
			this->Strings[i][j] = Q.Strings[i][j];
		}
		this->Strings[i][strlen(Q.Strings[i])] = '\0';
	}

	for (i = 0; i <= Q.IntK - 1; i++)
	{
		this->Ints[i] = Q.Ints[i];
	}
	for (i = 0; i <= Q.DoubleK - 1; i++)
	{
		this->Doubles[i] = Q.Doubles[i];
	}
	for (i = 0; i <= Q.StringK - 1; i++)
	{
		int j;
		this->StringName[i] = new char[strlen(Q.StringName[i]) + 1];
		for (j = 0; j <= strlen(Q.StringName[i]); j++)
		{
			this->Strings[i][j] = Q.Strings[i][j];
		}
		this->StringName[i][strlen(Q.StringName[i])] = '\0';
	}
	for (i = 0; i <= Q.DoubleK - 1; i++)
	{
		int j;
		this->DoubleName[i] = new char[strlen(Q.DoubleName[i]) + 1];
		for (j = 0; j <= strlen(Q.DoubleName[i]); j++)
		{
			this->DoubleName[i][j] = Q.DoubleName[i][j];
		}
		this->DoubleName[i][strlen(Q.DoubleName[i])] = '\0';
	}
	for (i = 0; i <= Q.IntK - 1; i++)
	{
		int j;
		this->IntName[i] = new char[strlen(Q.IntName[i]) + 1];
		for (j = 0; j <= strlen(Q.IntName[i]); j++)
		{
			this->IntName[i][j] = Q.IntName[i][j];
		}
		this->IntName[i][strlen(Q.IntName[i])] = '\0';
	}
}

CConfig & CConfig::operator=(const CConfig &Q)
{
	int i;
	this->IntK = Q.IntK;
	this->DoubleK = Q.DoubleK;
	this->StringK = Q.StringK;
	this->IntN = Q.IntN;
	this->DoubleN = Q.DoubleN;
	this->StringN = Q.StringN;
	this->StringName = new char*[Q.StringN];
	this->IntName = new char*[Q.IntN];
	this->DoubleName = new char*[Q.DoubleN];
	this->Ints = new int[Q.IntN];
	this->Doubles = new double[Q.DoubleN];
	this->Strings = new char*[Q.StringN];

	for (i = 0; i <= Q.StringK - 1; i++)
	{
		int j;
		this->Strings[i] = new char[strlen(Q.Strings[i]) + 1];
		for (j = 0; j <= strlen(Q.Strings[i]); j++)
		{
			this->Strings[i][j] = Q.Strings[i][j];
		}
		this->Strings[i][strlen(Q.Strings[i])] = '\0';
	}

	for (i = 0; i <= Q.IntK - 1; i++)
	{
		this->Ints[i] = Q.Ints[i];
	}
	for (i = 0; i <= Q.DoubleK - 1; i++)
	{
		this->Doubles[i] = Q.Doubles[i];
	}
	for (i = 0; i <= Q.StringK - 1; i++)
	{
		int j;
		this->StringName[i] = new char[strlen(Q.StringName[i]) + 1];
		for (j = 0; j <= strlen(Q.StringName[i]); j++)
		{
			this->StringName[i][j] = Q.StringName[i][j];
		}
		this->StringName[i][strlen(Q.StringName[i])] = '\0';
	}
	for (i = 0; i <= Q.DoubleK - 1; i++)
	{
		int j;
		this->DoubleName[i] = new char[strlen(Q.DoubleName[i]) + 1];
		for (j = 0; j <= strlen(Q.DoubleName[i]); j++)
		{
			this->DoubleName[i][j] = Q.DoubleName[i][j];
		}
		this->DoubleName[i][strlen(Q.DoubleName[i])] = '\0';
	}
	for (i = 0; i <= Q.IntK - 1; i++)
	{
		int j;
		this->IntName[i] = new char[strlen(Q.IntName[i]) + 1];
		for (j = 0; j <= strlen(Q.IntName[i]); j++)
		{
			this->IntName[i][j] = Q.IntName[i][j];
		}
		this->IntName[i][strlen(Q.IntName[i])] = '\0';
	}
	return (*this);
}

int CConfig::ToPush(char **str)
{
	char *s = *str;
	int n = strlen(s); // s[n] = '\0'
	int i;
	for (i = 0; i <= n - 1; i++)
	{
		if (s[i] == '$')
		{
			int j = i + 2;
			int k; // счетчик
			int p;
			char *c;
			//выделяем имя переменной 
			while (s[j] != ')')
			{
				j++;
			}
			// получаем фрагмент i+2 ... j-1 имя переменной, длина переменной j-i-2
			p = j - i - 2;
			c = s + i + 2;
			for (k = 0; k <= this->IntK - 1; k++)
			{
				if (strnlen(c, p) == strnlen(this->IntName[k], p))
				{
					if (strncmp(c, IntName[k], p) == 0)
					{
						char *dop;
						char *result;
						dop = new char[(int)(floor(log10(Ints[k])) + 4)];
						sprintf(dop, "(%d)", Ints[k]);
						insert(dop, i, j, str);
						delete[] dop;
						return this->ToPush(str);
					}
				}
			}
			for (k = 0; k <= this->DoubleK - 1; k++)
			{
				if (strnlen(c, p) == strnlen(this->DoubleName[k], p))
				{
					if (strncmp(c, DoubleName[k], p) == 0)
					{
						char *dop;
						dop = new char[(int)(floor(log10(Doubles[k])) + 10)];
						sprintf(dop, "(%.5lf)", Doubles[k]);
						insert(dop, i, j, str);
						delete[] dop;
						return this->ToPush(str);
					}
				}
			}
			for (k = 0; k <= this->StringK - 1; k++)
			{
				if (strnlen(c, p) == strnlen(this->StringName[k], p))
				{
					if (strncmp(c, StringName[k], p) == 0)
					{
						insert(Strings[k], i, j, str);
						return this->ToPush(str);
					}
				}
			}
		}
	}
}
//*/

int CConfig::Add(const char *str, int q)
{
	char *s = new char[strlen(str) + 1];
	strcpy(s, str);
	int i = 0;
	int k = 0;
	int n = strlen(s);
	char *VarName;
	char *Val;
	Trim(&s);
	while(s[k] != '=')
	{
		k++;
	}
	// 0 ... k - 1 имя = k + 1 ... n - 1 значение
	VarName = new char[k + 1];
	Val = new char[n - k];
	for (i = 0; i <= k - 1; i++)
	{
		VarName[i] = s[i];
	}
	VarName[k] = '\0';
	for (i = k + 1; i <= n - 1; i++)
	{
		Val[i - k - 1] = s[i];
	}
	Val[n - k - 1] = '\0';
	if (WhatType(VarName) == 0)
	{
		switch(q)
		{
			case 1 : // int
			{
				char *dop;
				dop = new char[strlen(Val) + 1];
				SEqInt Q;
				strcpy(dop, Val);
				this->ToPush(&dop);
				if (CreateSEqInt(&Q, dop) == -1)
				{
					break;
				}
				this->IntK++;
				this->Ints[this->IntK - 1] = IntEval(&Q);
				this->IntName[this->IntK - 1] = new char[strlen(VarName) + 1];
				strcpy(this->IntName[this->IntK - 1], VarName);
				DeleteSEqInt(&Q);
				delete[] dop;
				delete[] VarName;
				delete[] Val;
				delete[] s;
				break;
			}
			case 2 :
			{
				char *dop;
				dop = new char[strlen(Val) + 1];
				SEqDouble Q;
				strcpy(dop, Val);
				this->ToPush(&dop);
				if(CreateSEqDouble(&Q, dop) == -1)
				{
					break;
				}
				this->DoubleK++;
				this->Doubles[this->DoubleK - 1] = DoubleEval(&Q);
				this->DoubleName[this->DoubleK - 1] = new char[strlen(VarName) + 1];
				strcpy(this->DoubleName[this->DoubleK - 1], VarName);
				DeleteSEqDouble(&Q);
				delete[] dop;
				delete[] VarName;
				delete[] Val;
				delete[] s;
				break;
			}
			case 3 :
			{
				char *dop;
				dop = new char[strlen(Val) + 1];
				strcpy(dop, Val);
				this->ToPush(&dop);
				this->Strings[this->StringK] = new char[strlen(dop) + 1];
				strcpy(this->Strings[this->StringK], dop);
				this->Strings[this->StringK][strlen(dop)] = '\0';
				this->StringName[this->StringK] = new char[strlen(VarName) + 1];
				strcpy(this->StringName[this->StringK], VarName);
				this->StringK++;
				delete[] dop;
				delete[] VarName;
				delete[] Val;
				delete[] s;
				break;
			}
			default :
			{
				delete[] VarName;
				delete[] Val;
				delete[] s;
				return -1;
			}
		}
	}
	else
	{
		delete[] VarName;
		delete[] Val;
		delete[] s;
		return -1;
	}
	return 0;
}

CConfig::~CConfig(void)
{
	int i;
	for (i = 0; i <= IntK - 1; i++)
	{
		delete[] IntName[i];
	}
	for (i = 0; i <= DoubleK - 1; i++)
	{
		delete[] DoubleName[i];
	}
	for (i = 0; i <= StringK - 1; i++)
	{
		delete[] StringName[i];
		delete[] Strings[i];
	}

	delete[] IntName;
	delete[] DoubleName;
	delete[] StringName;
	delete[] Strings;
	delete[] Ints;
	delete[] Doubles;
}

CConfig::CConfig(const char *FileName)
{
	FILE *fp;
	fp = fopen(FileName, "r");
	int k = 0;
	int i;
	char c;
	int *len;
	while ((c = fgetc(fp)) != EOF)
	{
		if (c == '\n')
		{
			k++;
		}
	}
	len = new int[k];

	this->IntN = k;
	this->DoubleN = k;
	this->StringN = k;
	this->IntK = 0;
	this->DoubleK = 0;
	this->StringK = 0;
	this->IntName = new char*[k];
	this->DoubleName = new char*[k];
	this->StringName = new char*[k];
	this->Ints = new int[k];
	this->Doubles = new double[k];
	this->Strings = new char*[k];

	rewind(fp);
	for (i = 0; i <= k - 1; i++)
	{
		char d;
		char *dop;
		int len = 0;
		while((d = fgetc(fp)) != '\n')
		{
			if (d != EOF)
			{
				len++;
			}
			else
			{
				break;
			}
		}
		dop = new char[len + 1];
		fseek(fp, -len - 1, SEEK_CUR);
		fgets(dop, len + 1, fp);
		fgetc(fp);
		if (strncmp(dop, "int", 3) == 0)
		{
			this->Add(dop + 3, 1);
		}
		else if (strncmp(dop, "double", 6) == 0)
		{
			this->Add(dop + 6, 2);
		}
		else
		{
			this->Add(dop + 6, 3);
		}
		delete[] dop;
	}
	fclose(fp);
}

CConfig::CConfig(int n1, int n2, int n3)
{
	this->IntN = n1;
	this->DoubleN = n2;
	this->StringN = n3;
	this->IntK = 0;
	this->DoubleK = 0;
	this->StringK = 0;
	this->IntName = new char*[n1];
	this->DoubleName = new char*[n2];
	this->StringName = new char*[n3];
	this->Ints = new int[n1];
	this->Doubles = new double[n2];
	this->Strings = new char*[n3];
}

void CConfig::Print(void)
{
	int i;
	printf("Ints:\n");
	for (i = 0; i <= IntK - 1; i++)
	{
		printf("     %s : %d\n", IntName[i], Ints[i]);
	}
	printf("Doubles:\n");
	for (i = 0; i <= DoubleK - 1; i++)
	{
		printf("     %s : %lf\n", DoubleName[i], Doubles[i]);
	}
	printf("Strings:\n");
	for (i = 0; i <= StringK - 1; i++)
	{
		printf("     %s : %s\n", StringName[i], Strings[i]);
	}
}

