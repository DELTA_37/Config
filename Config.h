#include "math.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#pragma warning (disable : 4096)
#define min(a, b) ((a < b)? a : b)
class CConfig
{
private:
	int DoubleN;
	int IntN;
	int StringN;
	int DoubleK;
	int IntK;
	int StringK;
	double *Doubles;
	int *Ints;
	char **Strings;
	char **DoubleName;
	char **IntName;
	char **StringName;
public:
	CConfig(int n1, int n2, int n3);
	CConfig(const char *FileName);
	CConfig operator+(const CConfig &Q);
	CConfig & operator=(const CConfig &Q);
	CConfig(const CConfig &);
	~CConfig(void);
	int AddInt(const char *s, int x);
	int AddDouble(const char *s, double x);
	int AddString(const char *s, const char *dop);
	int WhatType(const char *s);
	int SetInt(const char *s, int x);
	int SetDouble(const char *s, double x);
	int SetString(const char *s, const char *x);
	int GetInt(const char *s, int *x);
	int GetDouble(const char *s, double *x);
	int GetString(const char *s, char **x);
	int ToPush(char **s);
	int Add(const char *, int q);
	void Print(void);
};
