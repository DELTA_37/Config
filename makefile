all: prog

prog: main.o analize.o Config.o
	g++ main.o analize.o Config.o -lm -g -o prog
main.o: main.cpp
	g++ main.cpp -c -g -o main.o
analize.o: analize.cpp analize.h
	g++ analize.cpp -c -g -o analize.o
Config.o: Config.cpp
	g++ Config.cpp -c -g -o Config.o
test: test.o analize.o Config.o
	g++ test.o analize.o Config.o -g -lm -o test
test.o: test.cpp
	g++ test.cpp -c -g -o test.o
clean:
	rm *.o
	rm prog
